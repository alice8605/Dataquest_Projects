Various Python projects to showcase acquired skills in Python programming.
Project topics include:
1) Learning the basics of Python data structures & learning to write simple functions and for loops for iterations 
2) Data preprocessing / cleaning 
3) Data visualizations 
4) Data analysis using Pandas
5) Querying data using Sqlite3 